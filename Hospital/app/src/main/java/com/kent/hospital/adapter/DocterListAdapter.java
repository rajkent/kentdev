package com.kent.hospital.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.kent.hospital.R;
import com.kent.hospital.model.user.Datum;

import java.util.List;

/**
 * Created by rbpatel on 3/16/2017.
 */

public class DocterListAdapter extends RecyclerView.Adapter<DocterListAdapter.ViewHolder> {
    private List<Datum> docters;
    private Context context;
    private int dateIndex=0;
    private OnBookAppClickListener onBookClickListener;

    public DocterListAdapter(List<Datum> docters, Context context) {
        this.docters = docters;
        this.context = context;
    }

    @Override
    public DocterListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = null;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.docter_list_items, viewGroup, false);


        return new DocterListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DocterListAdapter.ViewHolder holder, final int position) {
        holder.mDocterName.setText(docters.get(position).getName());
        holder.mDocterQual.setText(docters.get(position).getQualification());
        holder.mDocterExp.setText(docters.get(position).getExp()+" Years of Expirience");
//        holder.mDocterDesc.setText(docters.get(position).getQualification());
        holder.mDocterFee.setText("INR "+docters.get(position).getFee());
        holder.bookAp.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
    onBookClickListener.onBookAPClicked(position);
    }
});
//        if(position>0){
//            holder.mCustomTextView.setVisibility(View.GONE);
//        }



    }

    public void setOnBookClickListener(OnBookAppClickListener onBookClickListener) {
    this.onBookClickListener= onBookClickListener;
    }

    @Override
    public int getItemCount() {
        return docters.size();
    }

    public void setData(List<Datum> docters) {
        this.docters = docters;
    }

    public void setDateIndex(int dateIndex) {
        this.dateIndex = dateIndex;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mDocterName,mDocterQual,mDocterExp,mDocterFee,mDocterDesc;
        Button bookAp;
        public ViewHolder(View view) {
            super(view);
//            mCustomTextView = (CustomTextView) view.findViewById(R.id.up_ancher);
            mDocterName = (TextView) view.findViewById(R.id.dr_name);
            mDocterQual = (TextView) view.findViewById(R.id.dr_qualification);
            mDocterExp = (TextView) view.findViewById(R.id.dr_exp);
            mDocterDesc = (TextView) view.findViewById(R.id.dr_description);
            mDocterFee = (TextView) view.findViewById(R.id.dr_fee);
            bookAp = (Button) view.findViewById(R.id.bookAP);
        }
    }


    public interface OnBookAppClickListener {
        public void onBookAPClicked(int position);

    }


}
