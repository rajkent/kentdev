package com.kent.hospital;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kent.hospital.activities.BaseActivity;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.drfragments.AppointmentsFragment;
import com.kent.hospital.fragments.HomeFragment;
import com.kent.hospital.model.user.User;

public class LandingPageActivity extends BaseActivity
        implements View.OnClickListener {
    TextView homeTv, appoinemtsTv, pastAppointments,
            recmondtationsTv, settingsTv, logoutTv, nameTv, emailTv, mobileTv;
    private User user;
    ImageView progilePic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Virtu Helth");


        user = new Gson().fromJson(VirtuPrefs.getString(this, ApiConstants.LOGIN_DATA), User.class);

        nameTv = (TextView) findViewById(R.id.name);
        emailTv = (TextView) findViewById(R.id.email);
        mobileTv = (TextView) findViewById(R.id.mobile);


        nameTv.setText(user.getData().get(0).getName());
        emailTv.setText(user.getData().get(0).getEmail());
        mobileTv.setText(user.getData().get(0).getMobile());

        homeTv = (TextView) findViewById(R.id.home);
        appoinemtsTv = (TextView) findViewById(R.id.appointments);
        pastAppointments = (TextView) findViewById(R.id.past_appointments);
        recmondtationsTv = (TextView) findViewById(R.id.recomendations);
        settingsTv = (TextView) findViewById(R.id.settings);
        logoutTv = (TextView) findViewById(R.id.logout);


        homeTv.setOnClickListener(this);
        appoinemtsTv.setOnClickListener(this);
        pastAppointments.setOnClickListener(this);
        recmondtationsTv.setOnClickListener(this);
        settingsTv.setOnClickListener(this);
        logoutTv.setOnClickListener(this);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        replaceFragment(new HomeFragment());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.landing_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void replaceFragment(Fragment fragment) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment, fragment.getTag());
       // ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.home) {
            replaceFragment(new HomeFragment());
            // Handle the camera action
        } else if (id == R.id.appointments) {
            Fragment fragment =new AppointmentsFragment();
            Bundle b = new Bundle();
            b.putBoolean("pt",true);
            fragment.setArguments(b);
            replaceFragment(fragment);
        } else if (id == R.id.past_appointments) {

        } else if (id == R.id.recomendations) {

        } else if (id == R.id.settings) {

        } else if (id == R.id.logout) {
            Intent in = new Intent(this, SignInActivity.class);
            VirtuPrefs.putString(this, ApiConstants.LOGIN_DATA,"");
            startActivity(in);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
