package com.kent.hospital.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kent.hospital.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by rbpatel on 3/16/2017.
 */

public class SlotListAdapter extends RecyclerView.Adapter<SlotListAdapter.ViewHolder> {
    private  Map<String, Map<String, List<String>>> datesMap;
    private Context context;
    List<String> dateList;
    private OnBookClickListener onBookClickListener;

    Set<String> keys;
    public SlotListAdapter(Map<String, Map<String, List<String>>> datesMap, Context context) {
        this.datesMap = datesMap;
        this.context = context;
        keys = datesMap.keySet();
        dateList= new ArrayList<>();
        for (String d:keys){
            dateList.add(d);
        }
    }

    @Override
    public SlotListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.slot_items, viewGroup, false);
        return new SlotListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SlotListAdapter.ViewHolder holder, final int position) {
        holder.monthName.setText(getMonthName(dateList.get(position)));
        holder.dayName.setText(getDayName(dateList.get(position)));


        holder.morningL.removeAllViews();
        holder.afternoonL.removeAllViews();
        holder.eveningL.removeAllViews();
        for (String s:datesMap.get(dateList.get(position)).get("morning")){
            final Button t = new Button(context);
            t.setGravity(View.TEXT_ALIGNMENT_CENTER);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBookClickListener.onSlotClick(position,t.getText().toString(),dateList.get(position), holder.monthName.getText().toString());
                }
            });
            t.setText(s);
            holder.morningL.addView(t);
        }

        for (String s:datesMap.get(dateList.get(position)).get("afternoon")){
            final Button t = new Button(context);
            t.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            t.setGravity(View.TEXT_ALIGNMENT_CENTER);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBookClickListener.onSlotClick(position,t.getText().toString(),dateList.get(position), holder.monthName.getText().toString());
                }
            });
            t.setText(s);
            holder.afternoonL.addView(t);
        }

        for (String s:datesMap.get(dateList.get(position)).get("evening")){
            final Button t = new Button(context);
            t.setGravity(View.TEXT_ALIGNMENT_CENTER);
            t.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBookClickListener.onSlotClick(position,t.getText().toString(),dateList.get(position), holder.monthName.getText().toString());
                }
            });
            t.setText(s);
            holder.eveningL.addView(t);
        }


        holder.view.setTag(position);

    }

    public void setOnBookClickListener(OnBookClickListener onBookClickListener) {
    this.onBookClickListener= onBookClickListener;
    }


    @Override
    public int getItemCount() {
        return dateList.size();
    }

    public String getDayName(String date) {
        String name=null;
            try {
                Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(date);
               return  new SimpleDateFormat("EEE").format(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        return name;
    }

    public String getMonthName(String date) {
        String name=null;
        try {
            Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(date);
            return date.substring(0,2)+" "+new SimpleDateFormat("MMM").format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return name;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dayName,monthName;
        LinearLayout morningL,afternoonL,eveningL;
        Button bookAp;
        View view;
        public ViewHolder(View view) {
            super(view);
            this.view=view;
//            mCustomTextView = (CustomTextView) view.findViewById(R.id.up_ancher);
            dayName = (TextView) view.findViewById(R.id.day);
            monthName = (TextView) view.findViewById(R.id.date);
            morningL = (LinearLayout) view.findViewById(R.id.morning);
            afternoonL = (LinearLayout) view.findViewById(R.id.afternoon);
            eveningL = (LinearLayout) view.findViewById(R.id.evening);
        }
    }


    public interface OnBookClickListener {
        public void onBookAPClicked(int position);
        public void onSlotClick(int position,String slot,String appointment_date,String day);
        public void onScrollTime();

    }




}
