package com.kent.hospital;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.kent.hospital.activities.BookSlotActivity;

public class DoctorListView extends ListActivity {

    String strDepartment;
    String[] arDoctor = new String[2];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_doctor_list_view);
        System.out.println("chk11111111111111");
        strDepartment=getIntent().getExtras().getString("DEPARTMENT");


        System.out.println("chk222222222222222222222222222" + strDepartment);
        if(strDepartment.equalsIgnoreCase("Orthopaedic"))
        {
            arDoctor[0]="Dr. Abhishek Kumar";
            arDoctor[1]="Dr. Text";
        }

        if(strDepartment.equalsIgnoreCase("Cardiologist"))
        {
            arDoctor[0]="Dr. Vipul Verma";
            arDoctor[1]="Dr. Deepak Jain";
        }
        if(strDepartment.equalsIgnoreCase("Gynaecologist"))
        {
            arDoctor[0]="Dr. Komal Singh";
            arDoctor[1]="Dr. Text";
        }
        if(strDepartment.equalsIgnoreCase("Peditrician"))
        {
            arDoctor[0]="Dr. Vinesh Panwar";
            arDoctor[1]="Dr. Text";
            System.out.println("chk333333333333333333333");
        }
        if(strDepartment.equalsIgnoreCase("Neurologist"))
        {
            arDoctor[0]="Dr. Manish Gupta";
            arDoctor[1]="Dr. Text";
        }
        if(strDepartment.equalsIgnoreCase("Ophthalmologist"))
        {
            arDoctor[0]="Dr. Shweta Gupta";
            arDoctor[1]="Dr. Text";
        }  if(strDepartment.equalsIgnoreCase("General Surgeon"))
        {
            arDoctor[0]="Dr. Prashant Sikarwar";
            arDoctor[1]="Dr. Text";
        }
        if(strDepartment.equalsIgnoreCase("Physiotherapist"))
        {
            arDoctor[0]="Dr. Pranav Pandey";
            arDoctor[1]="Dr. Text";
        }  if(strDepartment.equalsIgnoreCase("Dietician"))
        {
            arDoctor[0]="Dr.Priyanka Dogra";
            arDoctor[1]="Dr. Text";
        }

       setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_department_list_view,arDoctor));

        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        System.out.println("*********Chk3");

        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(),
                        ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DoctorListView.this,BookSlotActivity.class);
                intent.putExtra("DOCTOR",((TextView) view).getText().toString());
                intent.putExtra("DEPARTMENT",strDepartment);

                startActivity(intent);
            }
        });

    }

}
