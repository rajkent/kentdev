package com.kent.hospital.constants;

/**
 * Created by rbpatel on 5/4/2017.
 */

public class ApiConstants {
    public static final String BASE_URL="http://virtuehealth.co.in/api/index.php";
    public static final String LOGIN_DATA ="login_data" ;
    public static final String PATIENT_ID = "patient_id";
    public static final String DR_ID = "dr_id";
    public static final String DP_ID = "dp_id";
    public static final String USER_DATA = "user";
    public static final String FCM_REG_ID = "regId";
    public static final String DEPARTMENT_DATA ="department" ;
}
