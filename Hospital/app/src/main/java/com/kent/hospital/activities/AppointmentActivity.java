package com.kent.hospital.activities;

import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import com.google.gson.Gson;
import com.kent.hospital.helper.NetworkService;
import com.kent.hospital.R;
import com.kent.hospital.adapter.SlotListAdapter;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.model.user.Datum;

//import com.google.gson.*;

public class AppointmentActivity extends BaseActivity implements SlotListAdapter.OnBookClickListener,NetworkService.OnNetworkCallBack {
    private static final int BOOK_APP =123 ;
    private NetworkService networkService;
    private TextView drName,drQualiF,drFee,drExp,timeSlot,dayName;
    Button proceedBtn;
    Calendar c;
    List<String> output;
    Map<String, Map<String, List<String>>> timeSlotMap;
    Map<String, List<String>> slots;
    List<String> morning, afternoon, evening;
    RecyclerView recyclerView;
    SlotListAdapter slotListAdapter;
    Intent intent;
    Datum user;
private com.kent.hospital.model.depts.Datum departments;

    String dp_id,dr_id,patientId,time_slot,appointment_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        recyclerView = (RecyclerView) findViewById(R.id.time_slots);
        drName = (TextView) findViewById(R.id.dr_name);
        drQualiF = (TextView) findViewById(R.id.dr_qualification);
        drExp = (TextView) findViewById(R.id.dr_exp);
        drFee = (TextView) findViewById(R.id.dr_fee);
        proceedBtn = (Button) findViewById(R.id.bookAP);
        timeSlot = (TextView) findViewById(R.id.selectedTime);
        dayName = (TextView) findViewById(R.id.daysName);
        timeSlotMap = new HashMap<>();
        intent = getIntent();
        dp_id = intent.getStringExtra(ApiConstants.DP_ID);
        dr_id = intent.getStringExtra(ApiConstants.DR_ID);
        patientId = intent.getStringExtra(ApiConstants.PATIENT_ID);
        departments =  new Gson().fromJson(intent.getStringExtra(ApiConstants.DEPARTMENT_DATA), com.kent.hospital.model.depts.Datum.class);

        user= new Gson().fromJson(intent.getStringExtra(ApiConstants.USER_DATA),Datum.class);

        if(user!=null){
            drName.setText(user.getName());
            drQualiF.setText(user.getQualification());
            drExp.setText(user.getExp()+ " Years of Expirience");
            drFee.setText("INR "+user.getFee());
        }
        slots = new HashMap<>();
        c = Calendar.getInstance();
        initSlot();
        getCalendar();

        proceedBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject obj = new JSONObject();
                if(!TextUtils.isEmpty(time_slot))
                {
                    try {
                        obj.put("action", "post");
                        obj.put("patient_id", patientId);
                        obj.put("user_id", dr_id);
                        obj.put("dp_id", dp_id);
                        obj.put("time_slot", time_slot);
                        obj.put("appointment_date", appointment_date);
                        obj.put("type","appointment");
                        makeServiceCall(obj.toString(), BOOK_APP);
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }else
                {
                    Toast.makeText(getApplicationContext(), "Please select time!", Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    private void makeServiceCall(String data, int requestCOde) {
        networkService = new NetworkService();
        if (networkService.isNetworkAvailable(this)) {
            networkService.setCallBack(this);
            networkService.setParameters(data.toString(), requestCOde);
           showProgress("Please wait...");
            networkService.execute();
        } else {
            Toast.makeText(this, "Please check your internet and try again!", Toast.LENGTH_LONG).show();
        }

    }
    private void initSlot() {

        if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("morning")){
            morning = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.morning)));
        }else if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("morning_dt")) {
            morning = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.morning_dt)));
        }else if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("morning_ot")) {
            morning = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.morning_ot)));
        }
        if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("Afternoon")){
            afternoon = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.afternoon)));
        }

        if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("evening")){
            evening = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.evening)));
        }else if(departments.getMorningSlot().length()>0 && departments.getMorningSlot().equalsIgnoreCase("evening_gc")) {
            evening = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.evening_gc)));
        }


        if(morning.size()>0)
        slots.put("morning",morning);
        if(afternoon.size()>0)
        slots.put("afternoon",afternoon);
        if(evening.size()>0)
        slots.put("evening",evening);
    }


    public Map<String, Map<String, List<String>>> getCalendar() {

        output = new ArrayList<String>();
        //Get current Day of week and Apply suitable offset to bring the new calendar
        //back to the appropriate Monday, i.e. this week or next
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                c.add(Calendar.DATE, 1);
                break;

            case Calendar.MONDAY:
                //Don't need to do anything on a Monday
                //included only for completeness
                //c.add(Calendar.DATE, 0);
                break;

            case Calendar.TUESDAY:
                c.add(Calendar.DATE, -1);
                break;

            case Calendar.WEDNESDAY:
                c.add(Calendar.DATE, -2);
                break;

            case Calendar.THURSDAY:
                c.add(Calendar.DATE, -3);
                break;

            case Calendar.FRIDAY:
                c.add(Calendar.DATE, -4);
                break;

            case Calendar.SATURDAY:
                c.add(Calendar.DATE, 2);
                break;
        }

        //Add the Monday to the output


        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");//dd/MM/yyyy
        Date date1 = new Date(c.getTimeInMillis());
        output.add(sdfDate.format(date1));
        timeSlotMap.put(sdfDate.format(date1), slots);
        for (int x = 0; x <= 6; x++) {
            //Add the remaining days to the output
            c.add(Calendar.DATE, 1);
            Date date = new Date(c.getTimeInMillis());
            output.add(sdfDate.format(date));
            timeSlotMap.put(sdfDate.format(date), slots);
        }

        prepareDocterList();
        return timeSlotMap;
    }

    private void prepareDocterList() {


        slotListAdapter = new SlotListAdapter(timeSlotMap, getApplicationContext());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        mLayoutManager.canScrollVertically();

        recyclerView.setLayoutManager(mLayoutManager);


       slotListAdapter.setOnBookClickListener(this);
        recyclerView.setAdapter(slotListAdapter);
        slotListAdapter.notifyDataSetChanged();


    }


    @Override
    public void onBookAPClicked(int position) {

    }

    @Override
    public void onSlotClick(int position ,String time_slot,String appointment_date,String day) {
        timeSlot.setText("Time :"+time_slot);
        dayName.setText("Date :"+day+", "+appointment_date);
        this.appointment_date=appointment_date;
        this.time_slot=time_slot;

    }

    @Override
    public void onScrollTime() {


    }


    @Override
    public void onSuccess(String data, int requestCode) {
        dismissProgress();

        if(!TextUtils.isEmpty(data)){
            try {
                JSONObject obj = new JSONObject(data);
                JSONObject dataObj= obj.getJSONObject("data");
                if(dataObj.optString("status").equalsIgnoreCase("success")){
                    Toast.makeText(this,dataObj.optString("message"),Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Toast.makeText(this,dataObj.optString("message"),Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}