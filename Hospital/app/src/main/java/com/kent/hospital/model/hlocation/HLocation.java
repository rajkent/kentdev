package com.kent.hospital.model.hlocation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HLocation {

@SerializedName("id")
@Expose
private String id;
@SerializedName("lat")
@Expose
private String lat;
@SerializedName("lng")
@Expose
private String lng;
@SerializedName("createdAt")
@Expose
private String createdAt;
@SerializedName("location_name")
@Expose
private String locationName;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getLat() {
return lat;
}

public void setLat(String lat) {
this.lat = lat;
}

public String getLng() {
return lng;
}

public void setLng(String lng) {
this.lng = lng;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getLocationName() {
return locationName;
}

public void setLocationName(String locationName) {
this.locationName = locationName;
}

}