package com.kent.hospital.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;
import com.kent.hospital.LandingPageActivity;
import com.kent.hospital.R;
import com.kent.hospital.SignInActivity;
import com.kent.hospital.VirtuPrefs;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.model.user.User;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if(VirtuPrefs.getString(getApplicationContext(), ApiConstants.LOGIN_DATA).length()>0){

                    if (new Gson().fromJson(VirtuPrefs.getString(getApplicationContext(), ApiConstants.LOGIN_DATA), User.class).getData().get(0).getRole().equalsIgnoreCase("2")){
                        Intent i = new Intent(SplashActivity.this, DocterLandingPage.class);
                        startActivity(i);
                    }else{
                        Intent i = new Intent(SplashActivity.this, LandingPageActivity.class);
                        startActivity(i);
                    }

                }else{
                    Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                    startActivity(i);
                }


                // close this activity
                finish();
            }
        }, 400);

    }
}
