package com.kent.hospital;

import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.content.Intent;

import com.kent.hospital.activities.AppointmentActivity;

public class VirtueHealthFinal extends AppCompatActivity {

    Button btnLogin,btnSignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtue_health_final);
        addListenerOnLoginButton();
        addListenerOnSignInButton();

    }


    public void addListenerOnLoginButton() {

        btnLogin = (Button) findViewById(R.id.Login);

        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(VirtueHealthFinal.this,
                        "OnClickListener : " +
                                "\nLogin : "+ String.valueOf(btnLogin.getText()),
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(VirtueHealthFinal.this,AppointmentActivity.class);
                startActivity(intent);

            }

        });


    }


    public void addListenerOnSignInButton() {

        btnSignIn = (Button) findViewById(R.id.SignIn);

        btnSignIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(VirtueHealthFinal.this,
                        "OnClickListener : " +
                                "\nSignIn : "+ String.valueOf(btnSignIn.getText()),
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(VirtueHealthFinal.this,SignInActivity.class);
                startActivity(intent);

            }

        });
    }




}
