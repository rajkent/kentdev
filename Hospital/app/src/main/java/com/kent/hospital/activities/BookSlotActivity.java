package com.kent.hospital.activities;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.kent.hospital.R;


public class BookSlotActivity extends AppCompatActivity {

    private TextView Output;
    private Button changeDate;

    Button six,seven,eight,sixten,sixtwe,sixthir,sixfor,sixfif;
    Button sevten,sevtwe,sevthir,sevfor,sevfif;
    Button eightten,eighttwe,eightthir,eightfor,eightfif;

    private int year;
    private int month;
    private int day;
    ArrayList alBookingSlot = new ArrayList();
    String strDepartment,strDoctor,strBookDate;

    static final int DATE_PICKER_ID = 1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_slot);

        strDepartment=getIntent().getExtras().getString("DEPARTMENT");
        strDoctor=getIntent().getExtras().getString("DOCTOR");

        Output = (TextView) findViewById(R.id.Output);
        changeDate = (Button) findViewById(R.id.changeDate);


        // Get current date by calender

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        month=month+1;
        day = c.get(Calendar.DAY_OF_MONTH);

        // Show current date
        strBookDate = day +"-"+month+"-"+year+"";
        Output.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(day).append("-").append(month).append("-")
                .append(year));

        // Button listener to show date picker dialog

        changeDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // On button click show datepicker dialog
                showDialog(DATE_PICKER_ID);

            }

        });

        six = (Button) findViewById(R.id.six);
        sixten = (Button) findViewById(R.id.sixten);
        sixtwe = (Button) findViewById(R.id.sixtwe);
        sixthir = (Button) findViewById(R.id.sixthir);
        sixfor = (Button) findViewById(R.id.sixfor);
        sixfif = (Button) findViewById(R.id.sixfif);

        seven = (Button) findViewById(R.id.seven);
        sevten = (Button) findViewById(R.id.sevten);
        sevtwe = (Button) findViewById(R.id.sevtwe);
        sevthir = (Button) findViewById(R.id.sevthir);
        sevfor = (Button) findViewById(R.id.sevfor);
        sevfif = (Button) findViewById(R.id.sevfif);

        eight = (Button) findViewById(R.id.eight);
        eightten = (Button) findViewById(R.id.eightten);
        eighttwe = (Button) findViewById(R.id.eighttwe);
        eightthir = (Button) findViewById(R.id.eightthir);
        eightfor = (Button) findViewById(R.id.eightfor);
        eightfif = (Button) findViewById(R.id.eightfif);

        matchTimeSlot();

        confirmBookTimeSlot();
    }

        public void matchTimeSlot() {

            alBookingSlot.add("18:10");
            alBookingSlot.add("18:50");
            alBookingSlot.add("19:10");
            alBookingSlot.add("19:20");
            alBookingSlot.add("19:40");


            for (int i = 0; i < alBookingSlot.size(); i++) {
                System.out.println(" for loop = " + alBookingSlot.get(i).toString());
                if (alBookingSlot.get(i).toString().equals("18:00")) {
                    six.setEnabled(false);
                    six.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("18:10")) {
                    sixten.setEnabled(false);
                    sixten.setTextColor(Color.parseColor("#FF0000"));

                }
                if (alBookingSlot.get(i).toString().equals("18:20")) {
                    sixtwe.setEnabled(false);
                    sixtwe.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("18:30")) {
                    sixthir.setEnabled(false);
                    sixthir.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("18:40")) {
                    sixfor.setEnabled(false);
                    sixfor.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("18:50")) {
                    sixfif.setEnabled(false);
                    sixfif.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:00")) {
                    seven.setEnabled(false);
                    seven.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:10")) {
                    sevten.setEnabled(false);
                    sevten.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:20")) {
                    sevtwe.setEnabled(false);
                    sevtwe.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:30")) {
                    sevthir.setEnabled(false);
                    sevthir.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:40")) {
                    sevfor.setEnabled(false);
                    sevfor.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("19:50")) {
                    sevfif.setEnabled(false);
                    sevfif.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:00")) {
                    eight.setEnabled(false);
                    eight.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:10")) {
                    eightten.setEnabled(false);
                    eightten.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:20")) {
                    eighttwe.setEnabled(false);
                    eighttwe.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:30")) {
                    eightthir.setEnabled(false);
                    eightthir.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:40")) {
                    eightfor.setEnabled(false);
                    eightfor.setTextColor(Color.parseColor("#FF0000"));
                }
                if (alBookingSlot.get(i).toString().equals("20:50")) {
                    eightfif.setEnabled(false);
                    eightfif.setTextColor(Color.parseColor("#FF0000"));
                }

            }
        }




    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // Show selected date
            Output.setText(new StringBuilder()
                    .append(day).append("-").append(month + 1).append("-")
                    .append(year));

        }
    };

    public void confirmBookTimeSlot()
        {

            six.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });

            sixten.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sixtwe.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sixthir.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sixfor.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sixfif.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            seven.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                   Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });

            sevten.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sevtwe.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sevthir.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sevfor.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            sevfif.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            eight.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                   Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });

            eightten.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            eighttwe.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            eightthir.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            eightfor.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);
                    startActivity(intent);
                }

            });
            eightfif.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(BookSlotActivity.this,Confirmation.class);
                    putDataIntent(intent);

                    startActivity(intent);
                }

            });



        }

    public void putDataIntent(Intent intent){

        intent.putExtra("DEPARTMENT",strDepartment);
        intent.putExtra("DOCTOR",strDoctor);

        intent.putExtra("BOOKDATE",strBookDate);

    }

}