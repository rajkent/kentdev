package com.kent.hospital.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import com.kent.hospital.R;
import com.kent.hospital.model.appointments.Datum;

import java.util.List;

/**
 * Created by rbpatel on 3/16/2017.
 */

public class AppointmentsListAdapter extends RecyclerView.Adapter<AppointmentsListAdapter.ViewHolder> {
    private List<Datum> appoinments;
    private Context context;
    private int dateIndex = 0;
    private OnBookAppClickListener onBookClickListener;

    public AppointmentsListAdapter(List<Datum> appoinments, Context context) {
        this.appoinments = appoinments;
        this.context = context;
    }

    @Override
    public AppointmentsListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = null;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.appointments_list_items, viewGroup, false);


        return new AppointmentsListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppointmentsListAdapter.ViewHolder holder, final int position) {
        holder.mDate.setText( appoinments.get(position).getAppointmentDate());
        holder.mSlot.setText(appoinments.get(position).getTimeSlot());
        holder.name.setText(appoinments.get(position).getName());
        holder.mobile.setText(appoinments.get(position).getMobile());
        holder.address.setText(appoinments.get(position).getAddress());


    }

    public void setOnBookClickListener(OnBookAppClickListener onBookClickListener) {
        this.onBookClickListener = onBookClickListener;
    }

    @Override
    public int getItemCount() {
        return appoinments.size();
    }

    public void setData(List<Datum> appoinments) {
        this.appoinments = appoinments;
    }

    public void setDateIndex(int dateIndex) {
        this.dateIndex = dateIndex;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mDate, mSlot, name, mobile, address;
        Button bookAp;

        public ViewHolder(View view) {
            super(view);
//            mCustomTextView = (CustomTextView) view.findViewById(R.id.up_ancher);
            mDate = (TextView) view.findViewById(R.id.date);
            mSlot = (TextView) view.findViewById(R.id.time_slots);
            name = (TextView) view.findViewById(R.id.name);
            mobile = (TextView) view.findViewById(R.id.mobile);
            address = (TextView) view.findViewById(R.id.address);
//            mDocterExp = (TextView) view.findViewById(R.id.dr_exp);
//            mDocterDesc = (TextView) view.findViewById(R.id.dr_description);
//            mDocterFee = (TextView) view.findViewById(R.id.dr_fee);
//            bookAp = (Button) view.findViewById(R.id.bookAP);
        }
    }


    public interface OnBookAppClickListener {
        public void onBookAPClicked(int position);

        public void onScroll();

    }


}
