package com.kent.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.app.ListActivity;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.kent.hospital.activities.AppointmentActivity;

public class DepartmentListView extends ListActivity {

    static final String[] DEPARTMENT = new String[] { "Orthopaedic Surgeon", "Physician & Clinical Cardiologist", "Obstetrician & Gynaecologist",
            "Peditrician", "Physician & Neurologist", "Ophthalmologist", "General Surgeon", "Physiotherapist",
            "Dietician"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_department_list_view);

      //  setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_department_list_view,DEPARTMENT));

        setListAdapter(new ArrayAdapter<String>(this,R.layout.activity_department_list_view,DEPARTMENT));



        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        System.out.println("*********Chk3");

        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
              Toast.makeText(getApplicationContext(),
                        ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(DepartmentListView.this,AppointmentActivity.class);
                intent.putExtra("DEPARTMENT",((TextView) view).getText().toString());
                startActivity(intent);
            }
        });



    }
}
