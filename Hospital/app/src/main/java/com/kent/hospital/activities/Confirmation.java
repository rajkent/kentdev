package com.kent.hospital.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kent.hospital.R;

public class Confirmation extends AppCompatActivity {

    String strDepartment,strBookDate,strDoctor;
    private TextView Dep_Name,Doc_Name;
    Button Submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        fetchData();

        Dep_Name = (TextView)findViewById(R.id.Dep_Name);
        Doc_Name = (TextView)findViewById(R.id.Doc_Name);
        setData();
        onClickSubmit();
    }

    public void fetchData(){
        strDepartment=getIntent().getExtras().getString("DEPARTMENT");
        strDoctor=getIntent().getExtras().getString("DOCTOR");
        strBookDate = getIntent().getExtras().getString("BOOKDATE");
            }

    public void setData(){
        Dep_Name.setText(strDepartment);
        System.out.println("strDoctor = "+ strDoctor);
        Doc_Name.setText(strDoctor);
    }

    public void onClickSubmit(){
        Submit = (Button) findViewById(R.id.Submit);

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastConfirmation();
            }
        });
    }

    public void broadcastConfirmation(){

        Intent intent = new Intent();
        intent.setAction("com.virtuehealth.virtuehealthfinal.CUSTOM_INTENT");
        intent.putExtra("DEPARTMENT",strDepartment);
        intent.putExtra("DOCTOR",strDoctor);

        intent.putExtra("BOOKDATE",strBookDate);
        sendBroadcast(intent);

    }
}