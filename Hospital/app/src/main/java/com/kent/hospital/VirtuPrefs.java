package com.kent.hospital;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rbpatel on 4/25/2017.
 */

public class VirtuPrefs {
    private static SharedPreferences sharedPreferences=null;
    private static final String KENT_SECURITY_PREFS="virtue";


    public static SharedPreferences getInstance(Context context){
        if(sharedPreferences==null){
            sharedPreferences= context.getSharedPreferences(KENT_SECURITY_PREFS,Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }


    public static String getString(Context context,String key){
        getInstance(context);
        return sharedPreferences.getString(key,"");
    }

    public static void putString(Context context,String key,String value){
        getInstance(context);
        sharedPreferences.edit().putString(key,value).commit();
    }


    public static boolean getBoolean(Context context,String key){
        getInstance(context);
        return sharedPreferences.getBoolean(key,false);
    }

    public static void putBoolean(Context context,String key,boolean value){
        getInstance(context);
        sharedPreferences.edit().putBoolean(key,value).commit();
    }


}
