package com.kent.hospital.drfragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kent.hospital.helper.NetworkService;
import com.kent.hospital.R;
import com.kent.hospital.VirtuPrefs;
import com.kent.hospital.activities.AppointmentActivity;
import com.kent.hospital.activities.BaseActivity;
import com.kent.hospital.adapter.AppointmentsListAdapter;
import com.kent.hospital.adapter.DocterListAdapter;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.model.appointments.Appointments;
import com.kent.hospital.model.hlocation.HospitalLocation;
import com.kent.hospital.model.user.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rbpatel on 5/4/2017.
 */

public class AppointmentsFragment extends Fragment implements NetworkService.OnNetworkCallBack, DocterListAdapter.OnBookAppClickListener {

    private View rootView;
    private NetworkService networkService;
    private Appointments appointments;
    private HospitalLocation hospitalLocation;
    private Spinner spinner, hLocation;
    private User doctors;
    private String locationId;
    private RecyclerView recyclerView;
    DocterListAdapter docterListAdapter;
    private static final int GET_APPOINTMENTS = 123;


    private User user;

    public AppointmentsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.dr_appointments_fragment, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        user = new Gson().fromJson(VirtuPrefs.getString(getActivity(), ApiConstants.LOGIN_DATA), User.class);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.dr_list);
        JSONObject obj = new JSONObject();
        try {

            obj.put("action", "get");
            if (getArguments().getBoolean("pt", false)) {
                obj.put("user_id", user.getData().get(0).getUserId());
            } else {
                obj.put("dr_id", user.getData().get(0).getUserId());
            }
            obj.put("type","appointment");
            makeServiceCall(obj.toString(), GET_APPOINTMENTS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void makeServiceCall(String data, int requestCOde) {
        networkService = new NetworkService();
        if (networkService.isNetworkAvailable(getActivity())) {
            networkService.setCallBack(this);
            networkService.setParameters(data.toString(), requestCOde);
            ((BaseActivity) getActivity()).showProgress("Please wait...");
            networkService.execute();
        } else {
            Toast.makeText(getActivity(), "Please check your internet and try again!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onSuccess(String data, int requestCode) {
        ((BaseActivity) getActivity()).dismissProgress();

        if (!TextUtils.isEmpty(data)) {

            switch (requestCode) {
                case GET_APPOINTMENTS:
                    appointments = new Gson().fromJson(data, Appointments.class);
                    if(appointments.getData().size()>0){
                        prepareDocterList();
                    }else{
                        Toast.makeText(getActivity(), "No appoinment found", Toast.LENGTH_LONG).show();

                    }

                    break;

                default:
                    break;

            }


        }
    }


    private void prepareDocterList() {


        AppointmentsListAdapter docterListAdapter = new AppointmentsListAdapter(appointments.getData(), getActivity());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        mLayoutManager.canScrollVertically();

        recyclerView.setLayoutManager(mLayoutManager);


        recyclerView.setAdapter(docterListAdapter);
        docterListAdapter.notifyDataSetChanged();


    }

    @Override
    public void onBookAPClicked(int position) {

        Intent in = new Intent(getActivity(), AppointmentActivity.class);
        startActivity(in);


    }
}
