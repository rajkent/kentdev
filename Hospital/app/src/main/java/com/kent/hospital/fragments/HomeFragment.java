package com.kent.hospital.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kent.hospital.VirtuPrefs;
import com.kent.hospital.activities.AppointmentActivity;
import com.kent.hospital.activities.BaseActivity;
import com.kent.hospital.helper.NetworkService;
import com.kent.hospital.R;
import com.kent.hospital.adapter.DocterListAdapter;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.model.depts.Datum;

import com.kent.hospital.model.depts.Depart;
import com.kent.hospital.model.hlocation.HLocation;
import com.kent.hospital.model.hlocation.HospitalLocation;
import com.kent.hospital.model.user.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rbpatel on 5/4/2017.
 */

public class HomeFragment extends Fragment implements NetworkService.OnNetworkCallBack, DocterListAdapter.OnBookAppClickListener {

    private View rootView;
    private NetworkService networkService;
    private Depart departMents;
    private HospitalLocation hospitalLocation;
    private Spinner spinner, hLocation;
    private User doctors;
    private String locationId,dp_id;
    private Datum department;
    private RecyclerView recyclerView;
    DocterListAdapter docterListAdapter;
    private static final int GET_DEPT = 123;
    private static final int GET_DR = 124;
    private static final int GET_LOC = 125;
    private User user;

    public HomeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        user = new Gson().fromJson(VirtuPrefs.getString(getActivity(), ApiConstants.LOGIN_DATA), User.class);

        spinner = (Spinner) rootView.findViewById(R.id.departments);
        hLocation = (Spinner) rootView.findViewById(R.id.hlocation);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.dr_list);


        hLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                locationId = hospitalLocation.getData().get(position).getId();

                JSONObject obj = new JSONObject();
                try {
                    obj.put("action", "get");
                    obj.put("loc_id", locationId);
                    obj.put("type", "dp");
                    makeServiceCall(obj.toString(), GET_DEPT);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                JSONObject obj = new JSONObject();
                try {
                    obj.put("action", "getDoc");
                    obj.put("dp_id", departMents.getData().get(position).getDpId());
                    obj.put("loc_id", locationId);
                    dp_id=departMents.getData().get(position).getDpId();
                    department =departMents.getData().get(position);
                    makeServiceCall(obj.toString(), GET_DR);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        JSONObject loc = new JSONObject();
        try {
            loc.put("action", "get");
            loc.put("type", "loc");
            makeServiceCall(loc.toString(), GET_LOC);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject obj = new JSONObject();
        try {
            obj.put("action", "get");
            obj.put("type", "dp");
            makeServiceCall(obj.toString(), GET_DEPT);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void makeServiceCall(String data, int requestCOde) {
        networkService = new NetworkService();
        if (networkService.isNetworkAvailable(getActivity())) {
            networkService.setCallBack(this);
            networkService.setParameters(data.toString(), requestCOde);
            ((BaseActivity) getActivity()).showProgress("Please wait...");
            networkService.execute();
        } else {
            Toast.makeText(getActivity(), "Please check your internet and try again!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onSuccess(String data, int requestCode) {
        ((BaseActivity) getActivity()).dismissProgress();

        if (!TextUtils.isEmpty(data)) {

            switch (requestCode) {
                case GET_DEPT:
                    departMents = new Gson().fromJson(data, Depart.class);
                    if (departMents.getStatus().equalsIgnoreCase("true") && departMents.getData().size() > 0) {
                        List<String> depts = new ArrayList<>();

                        for (Datum department : departMents.getData()) {
                            depts.add(department.getName());
                        }
                        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, depts));
                    }
                    break;

                case GET_LOC:
                    hospitalLocation = new Gson().fromJson(data, HospitalLocation.class);
                    if (hospitalLocation.getStatus().equalsIgnoreCase("true") && hospitalLocation.getData().size() > 0) {
                        List<String> locs = new ArrayList<>();

                        for (HLocation location : hospitalLocation.getData()) {
                            locs.add(location.getLocationName());
                        }
                        hLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, locs));
                    }
                    break;
                case GET_DR:

                    doctors = new Gson().fromJson(data, User.class);
                    if (doctors.getStatus().equalsIgnoreCase("true") && doctors.getData().size() > 0) {
                        prepareDocterList();
                    }
                    break;
                default:
                    break;

            }


        }
    }


    private void prepareDocterList() {


        DocterListAdapter docterListAdapter = new DocterListAdapter(doctors.getData(), getActivity());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        mLayoutManager.canScrollVertically();

        recyclerView.setLayoutManager(mLayoutManager);


        docterListAdapter.setOnBookClickListener(this);
        docterListAdapter.setDateIndex(1);
        recyclerView.setAdapter(docterListAdapter);
        docterListAdapter.notifyDataSetChanged();


    }

    @Override
    public void onBookAPClicked(int position) {

        Intent in = new Intent(getActivity(), AppointmentActivity.class);
        in.putExtra(ApiConstants.PATIENT_ID, user.getData().get(0).getUserId());
        in.putExtra(ApiConstants.DR_ID,doctors.getData().get(position).getUserId());
        in.putExtra(ApiConstants.DP_ID,dp_id);
        in.putExtra(ApiConstants.USER_DATA,new Gson().toJson(doctors.getData().get(position)));
        in.putExtra(ApiConstants.DEPARTMENT_DATA,new Gson().toJson(department));
        startActivity(in);
    }
}
