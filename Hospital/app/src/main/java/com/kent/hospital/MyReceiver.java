package com.kent.hospital;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.kent.hospital.activities.Confirmation;

/**
 * Created by TutorialsPoint7 on 8/23/2016.
 */
public class MyReceiver extends BroadcastReceiver{
    String strDepartment,strDoctor,strBookDate;

    @Override

    public void onReceive(Context context, Intent intent) {

        String strNotification = "Hi! Your Appointment with "+intent.getExtras().getString("DOCTOR")+" is confirmed on "+intent.getExtras().getString("BOOKDATE");
        fetchData(intent);


        addNotification(strNotification,context);
        Toast.makeText(context,strNotification , Toast.LENGTH_LONG).show();
    }

    private void addNotification(String strNotification,Context context){
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.vhlogo)
                        .setContentTitle("Virtue Health")
                        .setContentText(strNotification);

        Intent notificationIntent = new Intent(context, Confirmation.class);
        notificationIntent.putExtra("DEPARTMENT",strDepartment);
        notificationIntent.putExtra("DOCTOR",strDoctor);
        notificationIntent.putExtra("BOOKDATE",strBookDate);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

}

    public void fetchData(Intent intent) {
        strDepartment = intent.getExtras().getString("DEPARTMENT");
        strDoctor = intent.getExtras().getString("DOCTOR");
        strBookDate = intent.getExtras().getString("BOOKDATE");
    }

}