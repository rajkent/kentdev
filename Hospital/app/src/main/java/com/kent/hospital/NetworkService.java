package com.kent.hospital;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.kent.hospital.constants.ApiConstants;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by rbpatel on 5/4/2017.
 */

public class NetworkService extends AsyncTask<Void, Void, String> {

    private String data;
    private int requestCode;
    private OnNetworkCallBack onNetworkCallBack;

    public interface OnNetworkCallBack {
        public void onSuccess(String data,int requestCode);
    }

    @Override
    protected String doInBackground(Void... params) {
        return postData(data);
    }


    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);
        onNetworkCallBack.onSuccess(data,requestCode);
    }


    public void setParameters(String data,int requestCode) {
        this.data = data;
        this.requestCode = requestCode;
    }

    public void setCallBack(OnNetworkCallBack onNetworkCallBack) {
        this.onNetworkCallBack = onNetworkCallBack;
    }


    private synchronized String postData(String data){
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String jsonResponse=null;
        try {
            URL url = new URL(ApiConstants.BASE_URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            // is output buffer writter
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
//set headers and method
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(data);
// json data
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
//input stream
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String inputLine;
            while ((inputLine = reader.readLine()) != null)
                buffer.append(inputLine + "\n");
            if (buffer.length() == 0) {
                // Stream was empty. No point in parsing.
                return null;
            }
            jsonResponse = buffer.toString();

                return jsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    /**
     * @return
     */
    public  boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
