package com.kent.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kent.hospital.activities.BaseActivity;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.helper.NetworkService;

import org.json.JSONException;
import org.json.JSONObject;


public class SignUpActivity extends BaseActivity implements NetworkService.OnNetworkCallBack {

    private TextView login;
    private EditText nameEdt, emailEdt, mobileEdt, passwordEdt, addressEdt;
    NetworkService networkService;
    private Button signUpBtn;
    private RadioGroup rdGroup;
    private RadioButton radioButtonPt,radioButtondoc,radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_sign_up);


        login = (TextView) findViewById(R.id.signIn);
        signUpBtn = (Button) findViewById(R.id.signUp);

        nameEdt = (EditText) findViewById(R.id.name);
        mobileEdt = (EditText) findViewById(R.id.mobile);
        emailEdt = (EditText) findViewById(R.id.email);
        passwordEdt = (EditText) findViewById(R.id.password);
        addressEdt = (EditText) findViewById(R.id.address);
        rdGroup = (RadioGroup) findViewById(R.id.rdGroup);
        radioButtonPt = (RadioButton) findViewById(R.id.patient);
        radioButtondoc = (RadioButton) findViewById(R.id.docter);


        radioButtondoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(true){

                }
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput()) {
                    JSONObject obj = new JSONObject();


                    try {
                        obj.put("action", "post");
                        obj.put("name", nameEdt.getText().toString());
                        obj.put("mobile", mobileEdt.getText().toString());
                        obj.put("email", emailEdt.getText().toString());
                        obj.put("password", passwordEdt.getText().toString());
                        obj.put("address", addressEdt.getText().toString());
                        obj.put("type", "user");
                        obj.put("active", "1");
                        if (radioButton != null && radioButton.getText().toString().equalsIgnoreCase("doctor")) {
                            obj.put("role", "2");
                            obj.put("status", "NotActivated");
                        } else {
                            obj.put("role", "3");
                            obj.put("status", "null");
                        }

                        obj.put("gcm_regId", VirtuPrefs.getString(getApplicationContext(), ApiConstants.FCM_REG_ID));
                        makeServiceCall(obj.toString());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }
        }
    }

    );


}

    private void makeServiceCall(String data) {
        networkService = new NetworkService();
        if (networkService.isNetworkAvailable(getApplicationContext())) {
            networkService.setCallBack(this);
            networkService.setParameters(data.toString(), 1);
            showProgress("Please wait...");
            networkService.execute();
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet and try again!", Toast.LENGTH_LONG).show();
        }

    }

    private boolean validateInput() {

        if (TextUtils.isEmpty(nameEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter name", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(mobileEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter mobile", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(emailEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter email", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(addressEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter address", Toast.LENGTH_LONG).show();
            return false;
        }


        int id = rdGroup.getCheckedRadioButtonId();

        if (id != 0) {
            radioButton = (RadioButton) findViewById(id);
        } else {
            Toast.makeText(getApplicationContext(), "Please select register as", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(String data, int requestCode) {
        dismissProgress();

        if (!TextUtils.isEmpty(data)) {
            try {
                JSONObject obj = new JSONObject(data);

                if (obj.optString("status").equalsIgnoreCase("success")) {
                    Toast.makeText(getApplicationContext(), obj.optString("message"), Toast.LENGTH_LONG).show();
                    Intent in = new Intent(SignUpActivity.this, SignInActivity.class);
                    startActivity(in);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), obj.optString("message"), Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}