package com.kent.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kent.hospital.activities.BaseActivity;
import com.kent.hospital.activities.DocterLandingPage;
import com.kent.hospital.constants.ApiConstants;
import com.kent.hospital.helper.NetworkService;
import com.kent.hospital.model.user.User;

import org.json.JSONException;
import org.json.JSONObject;


public class SignInActivity extends BaseActivity implements NetworkService.OnNetworkCallBack {

    private Button loginBtn;
    private TextView signUp;
    private NetworkService networkService;
    private EditText emailEdt, passwordEdt;


    //RAj

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        loginBtn = (Button) findViewById(R.id.login);
        signUp = (TextView) findViewById(R.id.signup);



        emailEdt = (EditText) findViewById(R.id.email);
        passwordEdt = (EditText) findViewById(R.id.password);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput()) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("action", "get");
                        obj.put("email", emailEdt.getText().toString());
                        obj.put("password", passwordEdt.getText().toString());
                        obj.put("type", "user");
                        makeServiceCall(obj.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        //Implicit submit by hit by faizan
        passwordEdt.setOnEditorActionListener(new EditText.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE)
                {
                    signUp.performClick();
                    return true;
                }
                return false;
            }
        });

    }


    private void makeServiceCall(String data) {
        networkService = new NetworkService();
        if (networkService.isNetworkAvailable(getApplicationContext())) {
            networkService.setCallBack(this);
            networkService.setParameters(data.toString(),1);
            showProgress("Please wait...");
            networkService.execute();
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet and try again!", Toast.LENGTH_LONG).show();
        }

    }

    private boolean validateInput() {

        if (TextUtils.isEmpty(emailEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter email", Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(passwordEdt.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess(String data,int requestCode) {
        dismissProgress();

        if (!TextUtils.isEmpty(data)) {
            Log.d("Responce",data);
            User model = new Gson().fromJson(data,User.class);
            if(model.getStatus().equalsIgnoreCase("true") && model.getData().size()>0){

                if(model.getData().get(0).getRole().equalsIgnoreCase("2")){
                    Intent in = new Intent(SignInActivity.this, DocterLandingPage.class);
                    startActivity(in);
                }else{
                    Intent in = new Intent(SignInActivity.this, LandingPageActivity.class);
                    startActivity(in);
                }
                VirtuPrefs.putString(getApplicationContext(), ApiConstants.LOGIN_DATA,data);

                finish();
            }else{
                Toast.makeText(getApplicationContext(),"Invalid credentials!",Toast.LENGTH_LONG).show();
            }
        }
    }




}