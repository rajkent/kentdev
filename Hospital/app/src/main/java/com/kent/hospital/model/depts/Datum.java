package com.kent.hospital.model.depts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("dp_id")
    @Expose
    private String dpId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("loc_id")
    @Expose
    private String locId;
    @SerializedName("Morning_Slot")
    @Expose
    private String morningSlot;
    @SerializedName("Afternoon_Slot")
    @Expose
    private String afternoonSlot;
    @SerializedName("Evening_Slot")
    @Expose
    private String eveningSlot;
    @SerializedName("Location_ID")
    @Expose
    private String locationID;
    @SerializedName("Department_ID")
    @Expose
    private String departmentID;

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    public String getMorningSlot() {
        return morningSlot;
    }

    public void setMorningSlot(String morningSlot) {
        this.morningSlot = morningSlot;
    }

    public String getAfternoonSlot() {
        return afternoonSlot;
    }

    public void setAfternoonSlot(String afternoonSlot) {
        this.afternoonSlot = afternoonSlot;
    }

    public String getEveningSlot() {
        return eveningSlot;
    }

    public void setEveningSlot(String eveningSlot) {
        this.eveningSlot = eveningSlot;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(String departmentID) {
        this.departmentID = departmentID;
    }

}